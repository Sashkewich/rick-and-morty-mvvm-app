package com.example.rick_and_morty_mvvm_app.character_page.db

import androidx.room.*
import com.example.rick_and_morty_mvvm_app.character_page.db.CharacterEntity.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME,
    indices = [
        Index("ID"),
        Index("NAME")
    ]
)
@TypeConverters(CharacterConverter::class)
data class CharacterEntity (
    @ColumnInfo(name = "COUNT")
    val count: Int,
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    val id: Int,
    @ColumnInfo(name = "NAME")
    val name: String,
    @ColumnInfo(name = "STATUS")
    val status: String,
    @ColumnInfo(name = "SPECIES")
    val species: String,
    @ColumnInfo(name = "TYPE")
    val type: String,
    @ColumnInfo(name = "GENDER")
    val gender: String,
    @ColumnInfo(name = "ORIGIN_NAME")
    val originName: String,
    @ColumnInfo(name = "ORIGIN_URL")
    val originUrl: String,
    @ColumnInfo(name = "LOCATION_NAME")
    val locationName: String,
    @ColumnInfo(name = "LOCATION_URL")
    val locationUrl: String,
    @ColumnInfo(name = "IMAGE")
    val image: String,
    @ColumnInfo(name = "CREATED")
    val created: String

) {
    companion object {
        const val TABLE_NAME = "characters"
    }
}