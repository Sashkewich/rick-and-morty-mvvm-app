package com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class LocationDataUI(
    val name: String,
    val url: String
) : Parcelable