package com.example.rick_and_morty_mvvm_app.character_page.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.rick_and_morty_mvvm_app.character_page.model.Character
import com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI.CharacterUI
import com.example.rick_and_morty_mvvm_app.databinding.ItemListBinding
import com.example.rick_and_morty_mvvm_app.utils.extensions.changeStatusColor
import com.squareup.picasso.Picasso
import kotlin.properties.Delegates

class CharacterListViewHolder(
    private val binding: ItemListBinding,
    private val clickOnItem: (CharacterUI) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    constructor(
        parent: ViewGroup, onClickItem: (CharacterUI) -> Unit
    ) : this(
        ItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false),
        onClickItem
    )

    private lateinit var characterName: String
    private lateinit var characterStatus: String
    private var characterId by Delegates.notNull<Int>()
    private lateinit var item: CharacterUI


    fun getData(item: CharacterUI) {
        characterName = item.name
        characterStatus = item.status
        characterId = item.id
        Picasso.get().load(item.image).into(binding.characterImg)
        this.item = item
    }

    fun onBind() {
        with(binding) {
            txtNameCharacter.text = characterName
            txtStatus.text = characterStatus
            txtStatus.changeStatusColor(characterStatus, itemView.context)
            txtIdCharacter.text = characterId.toString()
            Picasso.get().load(item.image).into(binding.characterImg)
        }

        itemView.setOnClickListener {
            clickOnItem(item)
        }
    }
}