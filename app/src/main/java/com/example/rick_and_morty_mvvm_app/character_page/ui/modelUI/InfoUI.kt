package com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class InfoUI(
    val count: Int,
    val pages: Int,
    val next: String?,
    val prev: String?
) : Parcelable