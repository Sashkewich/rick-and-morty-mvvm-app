package com.example.rick_and_morty_mvvm_app.character_page.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface CharacterListDao {
    @Query("SELECT EPISODE_URL FROM ${EpisodeEntity.TABLE_NAME} WHERE CHARACTER_ID = :characterId")
    fun getEpisodes(characterId: Int): Flow<List<String>>

    @Query("SELECT * FROM ${CharacterEntity.TABLE_NAME} ORDER BY ID ASC")
    fun getCharacters(): Flow<List<CharacterEntity>>

    @Insert(entity = EpisodeEntity::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun addEpisodes(episodes: List<EpisodeEntity>)

    @Insert(entity = CharacterEntity::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun addCharacters(characters: List<CharacterEntity>)
}