package com.example.rick_and_morty_mvvm_app.character_page.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = [CharacterEntity::class, EpisodeEntity::class],
    version = 1,
    exportSchema = true
)
abstract class CharacterListDatabase : RoomDatabase() {
    abstract fun getCharacterDao(): CharacterListDao

    companion object {
        @Volatile
        private var dbInstance: CharacterListDatabase? = null

        fun getAppDB(context: Context): CharacterListDatabase {
            if(dbInstance == null){
                dbInstance = Room.databaseBuilder(
                    context.applicationContext,
                    CharacterListDatabase::class.java,
                    "character_room_database"
                ).build()
            }

            return dbInstance!!
        }
    }
}