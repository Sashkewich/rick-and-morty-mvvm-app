package com.example.rick_and_morty_mvvm_app.character_page.ui

import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rick_and_morty_mvvm_app.R
import com.example.rick_and_morty_mvvm_app.character_detail_page.ui.CharacterDetailFragment
import com.example.rick_and_morty_mvvm_app.character_page.ui.adapter.CharacterListAdapter
import com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI.CharacterListUI
import com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI.Params
import com.example.rick_and_morty_mvvm_app.common.mvvm.BaseFragment
import com.example.rick_and_morty_mvvm_app.databinding.FragmentCharacterListBinding
import com.example.rick_and_morty_mvvm_app.utils.extensions.replaceScreen
import com.example.rick_and_morty_mvvm_app.utils.extensions.viewbinding.viewBinding
import com.example.rick_and_morty_mvvm_app.utils.ui.EndlessScrollListener
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

private const val BASE_DATA_CHARACTER_COUNT = 826

@AndroidEntryPoint
class CharacterListFragment : BaseFragment(R.layout.fragment_character_list) {
    private val binding: FragmentCharacterListBinding by viewBinding()
    private val viewModel: CharacterListViewModel by viewModels()
    private var charactersData: CharacterListUI? = null
    private lateinit var scrollListener: EndlessScrollListener
    private lateinit var layoutManager: LinearLayoutManager


    private val adapter: CharacterListAdapter by lazy {
        CharacterListAdapter { item ->
            replaceScreen(CharacterDetailFragment.newInstance(item))
            print(item)
        }
    }

    override fun initViews(view: View) {
        with(binding) {
            layoutManager = LinearLayoutManager(requireContext())
            scrollListener = EndlessScrollListener(layoutManager) { _, page ->
                viewModel.setCurrentPage(page)
            }
            recyclerViewCharacters.layoutManager = layoutManager
            recyclerViewCharacters.setHasFixedSize(true)
            recyclerViewCharacters.adapter = adapter
            adapter.onAttachedToRecyclerView(recyclerViewCharacters)
            recyclerViewCharacters.addOnScrollListener(scrollListener)
        }
    }

    override fun bind() {
        with(viewModel) {
            observe(page) { page ->
                getCharactersList(Params(page))
                getCharactersFromDb()
            }
            observe(charactersListStateFlow) { charactersListStateFlow ->
                Timber.i("/*/ obs -> $charactersListStateFlow")
                when {
                    charactersListStateFlow != charactersData -> {
                        charactersData = charactersListStateFlow
                        when (charactersData!!.count) {
                            BASE_DATA_CHARACTER_COUNT -> {
                                adapter.addData(charactersData!!.results)
                            }
                            else -> {
                                adapter.setCharacters(charactersData!!.results)
                                scrollListener.setTotalLoadedItems(charactersData!!.count)
                            }
                        }
                    }
                    else -> {
                        adapter.addData(emptyList())
                    }
                }
            }


            observe(loadingStateFlow) { isLoading ->
                binding.progressBar.isVisible = isLoading
            }

            observe(pagingState) { pagingState ->
                adapter.setPagingState(pagingState)
            }
        }
    }

}
