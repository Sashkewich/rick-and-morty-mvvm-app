package com.example.rick_and_morty_mvvm_app.character_page.model

data class LocationData(
    val name: String,
    val url: String
)