package com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CharacterUI(
    val id: Int,
    val name: String,
    val status: String,
    val species: String,
    val type: String,
    val gender: String,
    val origin: String,
    val location: String,
    val image: String,
    val episode: List<String>,
    ) : Parcelable