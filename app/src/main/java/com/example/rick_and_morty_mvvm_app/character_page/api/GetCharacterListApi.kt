package com.example.rick_and_morty_mvvm_app.character_page.api

import com.example.rick_and_morty_mvvm_app.character_page.api.models.CharacterListResponse
import com.example.rick_and_morty_mvvm_app.character_page.api.models.CharacterResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface GetCharacterListApi {
    @GET("character")
    suspend fun getAllCharacters(
        @Query("page") page: Int,
        @Query("name") name: String = "",
        @Query("status") status: String = "",
        @Query("species") species: String = "",
        @Query("type") type: String = "",
        @Query("gender") gender: String = "",
        ): CharacterListResponse
}