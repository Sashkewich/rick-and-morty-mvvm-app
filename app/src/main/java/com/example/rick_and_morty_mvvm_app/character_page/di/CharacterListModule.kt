package com.example.rick_and_morty_mvvm_app.character_page.di

import android.content.Context
import com.example.rick_and_morty_mvvm_app.character_page.api.GetCharacterListApi
import com.example.rick_and_morty_mvvm_app.character_page.db.CharacterListDao
import com.example.rick_and_morty_mvvm_app.character_page.db.CharacterListDatabase
import com.example.rick_and_morty_mvvm_app.character_page.interactor.CharacterListInteractor
import com.example.rick_and_morty_mvvm_app.character_page.repository.CharacterListLocalRepository
import com.example.rick_and_morty_mvvm_app.character_page.repository.CharacterListLocalRepositoryImpl
import com.example.rick_and_morty_mvvm_app.character_page.repository.CharacterListRemoteRepository
import com.example.rick_and_morty_mvvm_app.character_page.repository.CharacterListRemoteRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class CharacterListModule {
    @Binds
    abstract fun bindCharacterListRemoteRepository(
        remoteRepository: CharacterListRemoteRepositoryImpl
    ): CharacterListRemoteRepository

    @Binds
    abstract fun bindCharacterListLocalRepository(
        localRepository: CharacterListLocalRepositoryImpl
    ): CharacterListLocalRepository

    companion object {
        @Provides
        @Singleton
        fun getDatabase(
            @ApplicationContext context: Context
        ): CharacterListDatabase = CharacterListDatabase.getAppDB(context)


        @Provides
        @Singleton
        fun getDao(database: CharacterListDatabase): CharacterListDao = database.getCharacterDao()

        @Provides
        @Singleton
        fun provideApi(retrofit: Retrofit): GetCharacterListApi =
            retrofit.create(GetCharacterListApi::class.java)

        @Provides
        @Singleton
        fun provideInteractor(
            remoteRepository: CharacterListRemoteRepository,
            localRepository: CharacterListLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): CharacterListInteractor =
            CharacterListInteractor(remoteRepository, localRepository, defaultDispatcher)

        // Dispatchers Injection
        @IoDispatcher
        @Provides
        fun provideContextIo(): CoroutineDispatcher = Dispatchers.IO

        @DefaultDispatcher
        @Provides
        fun provideContextDefault(): CoroutineDispatcher = Dispatchers.Default

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class IoDispatcher

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class DefaultDispatcher

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class MainDispatcher

        @Retention(AnnotationRetention.BINARY)
        @Qualifier
        annotation class MainImmediateDispatcher
    }
}
