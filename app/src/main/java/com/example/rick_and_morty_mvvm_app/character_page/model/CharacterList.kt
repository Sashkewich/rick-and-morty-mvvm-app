package com.example.rick_and_morty_mvvm_app.character_page.model

data class CharacterList(
    val info: Info,
    val results: List<Character>
)