package com.example.rick_and_morty_mvvm_app.character_page.db

import androidx.room.TypeConverter
import com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI.CharacterUI
import com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI.LocationDataUI
import java.util.stream.Collectors

object CharacterConverter {
    @TypeConverter
    fun fromEpisode(episode: List<String>) =
        episode.stream().collect(Collectors.joining(","))!!

    @TypeConverter
    fun toEpisode(str: String) = str.split(",".toRegex()).dropLastWhile { it.isEmpty() }
}