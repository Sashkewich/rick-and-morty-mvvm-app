package com.example.rick_and_morty_mvvm_app.character_page.repository

import com.example.rick_and_morty_mvvm_app.character_page.db.CharacterListDao
import com.example.rick_and_morty_mvvm_app.character_page.db.CharacterEntity
import com.example.rick_and_morty_mvvm_app.character_page.db.EpisodeEntity
import com.example.rick_and_morty_mvvm_app.character_page.di.CharacterListModule.Companion.DefaultDispatcher
import com.example.rick_and_morty_mvvm_app.character_page.model.Character
import com.example.rick_and_morty_mvvm_app.character_page.model.CharacterList
import com.example.rick_and_morty_mvvm_app.character_page.model.LocationData
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import timber.log.Timber
import javax.inject.Inject


class CharacterListLocalRepositoryImpl @Inject constructor(
    private val characterListDao: CharacterListDao,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) : CharacterListLocalRepository {

    override suspend fun putCharacterList(data: CharacterList) {
        withContext(defaultDispatcher) {
            val charactersEntity = data.results.map {
                CharacterEntity(
                    count = data.info.count,
                    id = it.id,
                    name = it.name,
                    status = it.status,
                    species = it.species,
                    type = it.type,
                    gender = it.gender,
                    originName = it.origin.name,
                    originUrl = it.origin.url,
                    locationName = it.location.name,
                    locationUrl = it.location.url,
                    image = it.image,
                    created = it.created
                )
            }
            characterListDao.addCharacters(characters = charactersEntity)

            val episodesEntity: MutableList<EpisodeEntity> = mutableListOf()
            data.results.forEach { character ->
                episodesEntity += character.episode.map {
                    EpisodeEntity(
                        id = 0,
                        characterId = character.id,
                        episodeUrl = it
                    )
                }
            }

            characterListDao.addEpisodes(episodes = episodesEntity)
        }
    }

    override suspend fun getCharactersList(): Flow<List<Character>> {
        return withContext(defaultDispatcher) {
            characterListDao.getCharacters().map { list ->
                list.map {
                    Character(
                        id = it.id,
                        name = it.name,
                        status = it.status,
                        species = it.species,
                        type = it.type,
                        gender = it.gender,
                        origin = LocationData(it.originName, it.originUrl),
                        location = LocationData(it.locationName, it.locationUrl),
                        image = it.image,
                        episode = findEpisodes(it.id, this),
                        created = it.created
                    )
                }
            }
        }
    }

    private suspend fun findEpisodes(id: Int, scope: CoroutineScope): List<String> {
        val episode = MutableStateFlow(emptyList<String>())
        scope.launch {
            Timber.i("/*/ epis1 = > $episode")
            characterListDao.getEpisodes(id).collect { episodes ->
                episode.value = episodes
            }
            Timber.i("/*/ epis2 = > $episode")
        }
        return episode.value
    }

}
