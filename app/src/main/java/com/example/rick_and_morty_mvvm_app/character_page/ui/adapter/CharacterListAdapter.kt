package com.example.rick_and_morty_mvvm_app.character_page.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.rick_and_morty_mvvm_app.R
import com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI.CharacterUI
import com.example.rick_and_morty_mvvm_app.utils.ui.PagingState

private const val ONE = 1

class CharacterListAdapter(
    private val clickOnItem: (CharacterUI) -> Unit
) : ListAdapter<CharacterUI, CharacterListViewHolder>(DIFF_CALLBACK) {

    private var pagingState: PagingState = PagingState.Idle
    private val noAdditionalItemRequiredState = listOf(PagingState.Idle)

    private val data = mutableListOf<CharacterUI>()

    companion object {
        val DIFF_CALLBACK: DiffUtil.ItemCallback<CharacterUI> =
            object : DiffUtil.ItemCallback<CharacterUI>() {
                override fun areItemsTheSame(oldItem: CharacterUI, newItem: CharacterUI): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: CharacterUI,
                    newItem: CharacterUI
                ): Boolean {
                    return oldItem.name == newItem.name && oldItem.image == newItem.image
                }
            }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterListViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return CharacterListViewHolder(parent, clickOnItem)
    }

    override fun getItemViewType(position: Int): Int = when {
        !stateRequiresExtraItem(pagingState) || position < itemCount - ONE -> {
            R.layout.item_list
        }
        pagingState is PagingState.Loading || pagingState is PagingState.InitialLoading -> {
            R.layout.item_progressbar
        }
        else -> R.layout.item_error
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: CharacterListViewHolder, position: Int) {
        val listItem = getItem(position)
        holder.getData(listItem)
        holder.onBind()
    }

    fun setPagingState(newState: PagingState) {
        if (pagingState::class.java == newState::class.java) return
        val shouldHasExtraItem = stateRequiresExtraItem(newState)
        val hasExtraItem = stateRequiresExtraItem(pagingState)

        pagingState = newState

        // since item count is a function - cache its value.
        val count = itemCount
        when {
            hasExtraItem && shouldHasExtraItem -> {
                notifyItemChanged(count - ONE)
            }
            hasExtraItem && !shouldHasExtraItem -> {
                notifyItemRemoved(count - ONE)
            }
            !hasExtraItem && shouldHasExtraItem -> {
                notifyItemInserted(count)
            }
        }
    }

    private fun stateRequiresExtraItem(state: PagingState) = state !in noAdditionalItemRequiredState

    fun addData(items: List<CharacterUI>) {
        data.addAll(items)
        submitList(data)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setCharacters(items: List<CharacterUI>) {
        data.clear()
        data.addAll(items)
        notifyDataSetChanged()
    }
}
