package com.example.rick_and_morty_mvvm_app.character_page.model

import com.example.rick_and_morty_mvvm_app.character_page.api.models.*

object CharacterListConverter {
    fun convert(data: CharacterListResponse) =
        CharacterList(
            info = convert(data.info),
            results = convert(data.results)
        )

    private fun convert(info: InfoResponse) =
        Info(
            count = info.count,
            pages = info.pages,
            next = info.next,
            prev = info.prev
        )

    private fun convert(characters: List<CharacterResponse>) =
        characters.map {
            Character(
                id = it.id,
                name = it.name,
                status = it.status,
                species = it.species,
                type = convertEmptyString(it.type),
                gender = it.gender,
                origin = convert(it.origin),
                location = convert(it.location),
                image = it.image,
                episode = it.episode,
                created = it.created
            )
        }

    private fun convert(location: LocationDataResponse) =
        LocationData(
            name = location.name,
            url = location.url,
        )

    private fun convertEmptyString(string: String): String {
        return when (string) {
            "" -> return "-"
            else -> string
        }
    }


}