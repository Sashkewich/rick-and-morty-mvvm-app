package com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI

import com.example.rick_and_morty_mvvm_app.character_page.model.CharacterList
import com.example.rick_and_morty_mvvm_app.character_page.model.Character

object ModelUiConverter {
    fun convert(data: CharacterList) =
        CharacterListUI(
            count = data.info.count,
            results = convert(data.results)
        )

    private fun convert(characters: List<Character>) =
        characters.map {
            CharacterUI(
                id = it.id,
                name = it.name,
                status = it.status,
                species = it.species,
                type = convertEmptyString(it.type),
                gender = it.gender,
                origin = it.origin.name,
                location = it.location.name,
                image = it.image,
                episode = it.episode
            )
        }

    private fun convertEmptyString(string: String): String {
        return when (string) {
            "" -> return "-"
            else -> string
        }
    }

}