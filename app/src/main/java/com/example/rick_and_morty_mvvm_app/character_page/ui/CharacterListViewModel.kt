package com.example.rick_and_morty_mvvm_app.character_page.ui

import androidx.lifecycle.MutableLiveData
import com.example.rick_and_morty_mvvm_app.character_page.di.CharacterListModule.Companion.DefaultDispatcher
import com.example.rick_and_morty_mvvm_app.character_page.di.CharacterListModule.Companion.IoDispatcher
import com.example.rick_and_morty_mvvm_app.character_page.interactor.CharacterListInteractor
import com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI.CharacterListUI
import com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI.CharacterUI
import com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI.Params
import com.example.rick_and_morty_mvvm_app.common.mvvm.BaseViewModel
import com.example.rick_and_morty_mvvm_app.utils.ui.PagingState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import timber.log.Timber
import javax.inject.Inject

private const val START_PAGE = 0
private const val DEFAULT_VALUE = 0

@HiltViewModel
class CharacterListViewModel @Inject constructor(
    private val interactor: CharacterListInteractor,
    @DefaultDispatcher val defaultDispatcher: CoroutineDispatcher,
    @IoDispatcher val ioDispatcher: CoroutineDispatcher
) : BaseViewModel() {

    private var pagingJob: Job? = null
    private var paginationEnded: Boolean = false

    private val _charactersListStateFlow =
        MutableStateFlow(CharacterListUI(DEFAULT_VALUE, emptyList()))
    val charactersListStateFlow = _charactersListStateFlow.asStateFlow()


    private val _loadingStateFlow = MutableStateFlow(false)
    val loadingStateFlow = _loadingStateFlow.asStateFlow()

    private val _pagingState: MutableStateFlow<PagingState> = MutableStateFlow(PagingState.Idle)
    val pagingState = _pagingState.asStateFlow()

    private val _page = MutableStateFlow(START_PAGE)
    val page = _page.asStateFlow()


    fun getCharactersFromDb() {
        launch {
            withContext(defaultDispatcher) {
                Timber.i("init works")
                interactor.getAllCharactersList().collect { list ->
                    val data = CharacterListUI(826, list.map {
                        CharacterUI(
                            id = it.id,
                            name = it.name,
                            status = it.status,
                            species = it.species,
                            type = it.type,
                            gender = it.gender,
                            origin = it.origin.name,
                            location = it.location.name,
                            image = it.image,
                            episode = it.episode
                        )
                    })
                    Timber.i("/*/ data -> $data")
                    _charactersListStateFlow.tryEmit(data)
                    return@collect
                }

            }
        }
    }


    fun getCharactersList(params: Params) {
        _loadingStateFlow.tryEmit(true)
        paginationEnded = false
        _pagingState.tryEmit(PagingState.InitialLoading)
        pagingJob?.cancel()
        pagingJob = launch {
            try {
                withContext(ioDispatcher) {
                    interactor.putCharactersList(params)
                    paginationEnded = true
                }
                _pagingState.tryEmit(PagingState.Idle)
            } catch (e: CancellationException) {
                Timber.e("/*/ Error ${e.message}")
            } catch (t: Throwable) {
                Timber.e("/*/ Error ${t.message}")
            } finally {
                _loadingStateFlow.tryEmit(false)
            }
        }
    }

    fun setCurrentPage(page: Int) {
        _page.tryEmit(page)
    }

}
