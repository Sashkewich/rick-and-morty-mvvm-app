package com.example.rick_and_morty_mvvm_app.character_page.model

data class Info(
    val count: Int,
    val pages: Int,
    val next: String?,
    val prev: String?
)