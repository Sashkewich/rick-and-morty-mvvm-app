package com.example.rick_and_morty_mvvm_app.character_page.api.models

import com.google.gson.annotations.SerializedName

data class CharacterResponse(
    @SerializedName("created")
    val created: String,
    @SerializedName("episode")
    val episode: List<String>,
    @SerializedName("gender")
    val gender: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("image")
    val image: String,
    @SerializedName("location")
    val location: LocationDataResponse,
    @SerializedName("name")
    val name: String,
    @SerializedName("origin")
    val origin: LocationDataResponse,
    @SerializedName("species")
    val species: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("url")
    val url: String
)