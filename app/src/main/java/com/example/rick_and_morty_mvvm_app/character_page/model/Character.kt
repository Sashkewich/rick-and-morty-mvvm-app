package com.example.rick_and_morty_mvvm_app.character_page.model


data class Character(
    val created: String,
    val episode: List<String>,
    val gender: String,
    val id: Int,
    val image: String,
    val location: LocationData,
    val name: String,
    val origin: LocationData,
    val species: String,
    val status: String,
    val type: String,
)