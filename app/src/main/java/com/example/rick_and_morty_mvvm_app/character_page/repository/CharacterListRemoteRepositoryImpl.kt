package com.example.rick_and_morty_mvvm_app.character_page.repository

import com.example.rick_and_morty_mvvm_app.character_page.api.GetCharacterListApi
import com.example.rick_and_morty_mvvm_app.character_page.di.CharacterListModule
import com.example.rick_and_morty_mvvm_app.character_page.di.CharacterListModule.Companion.IoDispatcher
import com.example.rick_and_morty_mvvm_app.character_page.model.CharacterListConverter
import com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI.Params
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CharacterListRemoteRepositoryImpl @Inject constructor(
    private val api: GetCharacterListApi,
    @IoDispatcher private val IoDispatcher: CoroutineDispatcher
) : CharacterListRemoteRepository {
    override suspend fun getCharacterList(params: Params) =
        withContext(IoDispatcher) {
            CharacterListConverter.convert(
                api.getAllCharacters(
                    page = params.page,
                    name = params.name,
                    status = params.status,
                    species = params.species,
                    type = params.type,
                    gender = params.gender
                )
            )
        }
}
