package com.example.rick_and_morty_mvvm_app.character_page.api.models

import com.google.gson.annotations.SerializedName

data class LocationDataResponse(
    @SerializedName("name")
    val name: String,
    @SerializedName("url")
    val url: String
)