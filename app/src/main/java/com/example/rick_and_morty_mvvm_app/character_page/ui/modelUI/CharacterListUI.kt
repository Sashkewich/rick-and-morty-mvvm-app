package com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CharacterListUI(
    val count: Int,
    val results: List<CharacterUI>
) : Parcelable