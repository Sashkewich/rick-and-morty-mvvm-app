package com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI

data class Params(
    val page: Int = 0,
    val name: String = "",
    val status: String = "",
    val species: String = "",
    val type: String = "",
    val gender: String = ""
)