package com.example.rick_and_morty_mvvm_app.character_page.repository

import com.example.rick_and_morty_mvvm_app.character_page.db.CharacterEntity
import com.example.rick_and_morty_mvvm_app.character_page.model.CharacterList
import com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI.CharacterUI
import com.example.rick_and_morty_mvvm_app.character_page.model.Character
import kotlinx.coroutines.flow.Flow


interface CharacterListLocalRepository {
    suspend fun getCharactersList(): Flow<List<Character>>
    suspend fun putCharacterList(data : CharacterList)
}