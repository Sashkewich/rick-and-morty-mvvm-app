package com.example.rick_and_morty_mvvm_app.character_page.interactor

import com.example.rick_and_morty_mvvm_app.character_page.di.CharacterListModule
import com.example.rick_and_morty_mvvm_app.character_page.repository.CharacterListLocalRepository
import com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI.Params
import com.example.rick_and_morty_mvvm_app.character_page.repository.CharacterListRemoteRepository
import com.example.rick_and_morty_mvvm_app.character_page.model.Character
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CharacterListInteractor @Inject constructor(
    private val remoteRepository: CharacterListRemoteRepository,
    private val localRepository: CharacterListLocalRepository,
    @CharacterListModule.Companion.DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) {
    suspend fun getAllCharactersList(): Flow<List<Character>> {
        return withContext(defaultDispatcher) {
            localRepository.getCharactersList()
        }

    }

    suspend fun putCharactersList(params: Params) {
        val characters = remoteRepository.getCharacterList(params)
        localRepository.putCharacterList(characters)
    }
}
