package com.example.rick_and_morty_mvvm_app.character_page.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.example.rick_and_morty_mvvm_app.character_page.db.EpisodeEntity.Companion.TABLE_NAME

@Entity(
    tableName = TABLE_NAME,
    indices = [
        Index("CHARACTER_ID")
    ]
)
data class EpisodeEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    val id: Int,
    @ColumnInfo(name = "CHARACTER_ID")
    val characterId: Int,
    @ColumnInfo(name = "EPISODE_URL")
    val episodeUrl: String
) {
    companion object {
        const val TABLE_NAME = "episodes"
    }
}

