package com.example.rick_and_morty_mvvm_app.character_page.repository

import com.example.rick_and_morty_mvvm_app.character_page.model.CharacterList
import com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI.Params

interface CharacterListRemoteRepository {
    suspend fun getCharacterList(params: Params): CharacterList
}