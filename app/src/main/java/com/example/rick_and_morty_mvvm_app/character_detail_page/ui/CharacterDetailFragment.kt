package com.example.rick_and_morty_mvvm_app.character_detail_page.ui

import android.view.View
import com.example.rick_and_morty_mvvm_app.R
import com.example.rick_and_morty_mvvm_app.character_page.model.Character
import com.example.rick_and_morty_mvvm_app.character_page.ui.CharacterListFragment
import com.example.rick_and_morty_mvvm_app.character_page.ui.modelUI.CharacterUI
import com.example.rick_and_morty_mvvm_app.common.mvvm.BaseFragment
import com.example.rick_and_morty_mvvm_app.databinding.FragmentDetailBinding
import com.example.rick_and_morty_mvvm_app.utils.Arguments
import com.example.rick_and_morty_mvvm_app.utils.extensions.args
import com.example.rick_and_morty_mvvm_app.utils.extensions.changeStatusColor
import com.example.rick_and_morty_mvvm_app.utils.extensions.replaceScreen
import com.example.rick_and_morty_mvvm_app.utils.extensions.viewbinding.viewBinding
import com.example.rick_and_morty_mvvm_app.utils.extensions.withArgs
import com.squareup.picasso.Picasso

class CharacterDetailFragment : BaseFragment(R.layout.fragment_detail) {
    private val binding: FragmentDetailBinding by viewBinding()

    companion object {
        fun newInstance(character: CharacterUI) =
            CharacterDetailFragment()
                .withArgs(Arguments.CHARACTER_DATA to character)
    }

    private val character: CharacterUI by args(Arguments.CHARACTER_DATA)
    override fun bind() {
        with(binding) {
            txtIdCharacter.text = character.id.toString()
            txtStatus.text = character.status
            txtStatus.changeStatusColor(character.status, requireContext())
            Picasso.get().load(character.image).into(binding.imgCharacter)
            txtName.text = character.name
            txtSpecie.text = character.species
            txtGender.text = character.gender
            txtNEpisodes.text = character.episode.size.toString()
            txtOrigin.text = character.origin
            txtLocation.text = character.location
            backButton.setOnClickListener {
                replaceScreen(CharacterListFragment(), clearBackStack = true)
            }
        }
    }

    override fun initViews(view: View) {}
}
