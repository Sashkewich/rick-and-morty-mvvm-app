package com.example.rick_and_morty_mvvm_app.common.mvvm

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel : ViewModel() {
    protected fun launch(func: suspend CoroutineScope.() -> Unit): Job {
        return viewModelScope.launch (block = func)
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
}