package com.example.rick_and_morty_mvvm_app.utils.extensions

import android.content.Context
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.rick_and_morty_mvvm_app.R

fun TextView.changeStatusColor(status: String, context: Context) {
    when (status) {
        "Alive" -> setTextColor(
            ContextCompat.getColor(
                context,
                R.color.green_light
            )
        )
        "Dead" -> setTextColor(
            ContextCompat.getColor(
                context,
                R.color.red
            )
        )
        "unknown" -> setTextColor(
            ContextCompat.getColor(
                context,
                R.color.black_gray
            )
        )
    }
}

fun formatEpisodeNum(url: String) =
    buildString {
        append("Episode № ")
            .append(url.split("/").last())
    }

