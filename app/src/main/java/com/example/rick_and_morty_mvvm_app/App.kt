package com.example.rick_and_morty_mvvm_app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        setupTimber()
    }

    private fun setupTimber(){
        Timber.plant(Timber.DebugTree())
    }
}
