package com.example.rick_and_morty_mvvm_app.root

import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.rick_and_morty_mvvm_app.R
import com.example.rick_and_morty_mvvm_app.character_page.ui.CharacterListFragment
import com.example.rick_and_morty_mvvm_app.databinding.ActivityRootBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RootActivity : AppCompatActivity() {
//    var navController: NavController? = null
    private lateinit var binding: ActivityRootBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_RickAndMortyCardApp)
        supportActionBar?.hide()
        super.onCreate(savedInstanceState)
        binding = ActivityRootBinding.inflate(layoutInflater)
        setContentView(binding.root)
        replace(CharacterListFragment())
    }

    private fun replace(fragment: Fragment, tag: String = fragment::class.java.name) {
        val fragmentManager = this.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction
            .replace(R.id.container, fragment, tag)
            .addToBackStack(tag)
            .commit()
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Are you sure to exit from app?")
        builder.setCancelable(true)
        builder.setNegativeButton("No", DialogInterface.OnClickListener { dialog, _ ->
            dialog.cancel()
        })

        builder.setPositiveButton("Yes", DialogInterface.OnClickListener { _, _ ->
            finish()
        })

        val dialogAlert = builder.create()
        dialogAlert.show()
    }
}